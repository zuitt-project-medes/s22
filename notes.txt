Data Modeling

What is Data Model?
	>> A data model describes how data is organized or grouped in a database.

	>> A data model is like a blueprint for our database

	>> By creating data models, we can anticipate which data will be managed by the database management system in accordance to the application to be developed

Data Modelling Scenario
	Type: Course Booking System,
	Description: Students/ Users should be able to book into a course
	Type of Users: Regular Users (student/ clients), Admin Users (maintainer), (Instructors)
	hasCourses: true

	Features:
		-user registration
		-user authentication/ login
		-Features of a regular user:
			-view available courses
			-enroll in a course
			-update his own details
			-delete his optional details (except for credentials)
		-Features of Admin User:
			-create courses
			-update courses
			-archive courses
			-unarchive courses
			-view all courses

	users and courses as our collections

	users: 

	{
		(id)
		username,
		email,
		password,
		isAdmin,
		isActive,
		mobileNo,
		firstName,
		lastName,
		enrollments: [
			{
				course_id,
				course_name,
				dateEnrolled,
				status,
				isPaid
			}

		]

	}

	courses:

	{
		(id)
		name,
		description,
		price,
		isActive,
		instructor,
		classSchedule,
		units,
		slotsLeft,
		enrollees: [
			{
				student_id,
				firstName,
				lastName,
				status,
				dateEnrolled
			}
		]

	}

Model Relationship
	
	One to One- this relationship means that a model is exclusively related to only one model

	Employee: 
	{
		"id": "2021dev"
		"firstName" : "Jack",
		"lastName" : "Sparrow",
		"email" : "jacksparrow@mail.com"
	}

	Credentials:
	{
		"id" : "2021dev",
		"role" : "dev",
		"team" : "tech"
	}

	Embedded Subdocuments- subdocuments are documents embedded or inside a parent document

	user
	{
		"id" : "2021dev",
		"firstName" : "Jack",
		"lastName" : "Sparrow",
		"email" : "jacksparrow@mail.com",
		"credentials" : {

			"role" : "dev",
			"team" : "tech"
		}
	}

	One to Many- is related to multiple other models however the other models are only related to one

	Person- many email addresses
	Facebook Post- comments
		A single Facebook Post can have multiple comments but each comment should only refer to single post

	post 
	{
		"id" : "post03242022",
		"title" : "This is my first post",
		"content" : "Hi! This is my first post",
		"createdOn" : "03/24/2022",
		"author" : "yourAunt22"
	}

	comment documents
	{
		"id" : "comment01",
		"comment" : "Hi!",
		"author" : "commenter101",
		"post_id" : "post03242022"
	}

	{
		"id" : "comment02",
		"comment" : "Welcome!",
		"author" : "commenter102",
		"post_id" :  "post03242022"
	}

	Subdocument Array- an array of subdocuments per single document

	post with comments subdocument array

	{
		"id" : "post03242022",
		"title" : "This is my first post",
		"content" : "Hi! This is my first post",
		"createdOn" : "03/24/2022",
		"author" : "yourAunt22",
		"comments" : [
			{
				"id" : "comment01",
				"comment" : "Hi!",
				"author" : "commenter101",
			},
			{
				"id" : "comment02",
				"comment" : "Welcome!",
				"author" : "commenter102"
			}
		]
	}

	Many to Many- relationship of models wherein multiple documents are related to multiple documents

	Books and Authors

	id			name 			price
	1 			Origin			1500
	2 			Harry Potter	500

	id 			name
	1 			Dan Brown
	2 			J.K Rowling
	3   		Not Me

	Associate Entity is created when you have many to many relationship

	books_authors

	id 		book_id 	author_id
	1 		1 			1

	MongoDB- two way embedding, wherein we can embed details of documents embedded in other documents as these other document's details are also embedded in another

	books

	{

		"id" : "book1",
		"name" : "The Great Escape",
		"description" : "It talks about the great escape",
		"price" : "1500",
		"authors" : [
			{
				"id" : "bookAuthor1",
				"author_id" : "author1"
			},
			{
				"id" : "bookAuthor2",
				"author_id" : "author2"
			}
		]
	}

	{
		"id" : "book2",
		"name" : "The Unknown Escape",
		"description" : "Talks about the unknown escape",
		"price" : "1000",
		"authors" : [
			{
				"id" : "bookAuthor1",
				"author_id" : "author1"
			}
		]
	}

	{
		"id" : "book3",
		"name" : "The Unknown Escape: Revised",
		"description" : "Still the same just with more words",
		"price" : "2000",
		"authors" : [
			{
				"id" : "bookAuthor1",
				"author_id" : "author1"
			}
		]
	}

	authors
	{
		"id" : "author1",
		"name" : "Will Wilson",
		"address" : {
			"street" : "#1 Shakespeare Drive",
			"city" : "London",
			"country" : "UK"
		}

		"books" : [
			{	
				"id" : "authored_book1",
				"book_id" : "book1",
				"writtenOn" : "02/26/97"
			},
			{
				"id" : "authored_book2",
				"book_id" : "book2",
				"writtenOn" : "03/23/98"
			},
			{
				"id" : "authored_book3",
				"book_id" : "book3",
				"writtenOn" : "09/23/99"
			},
		]
	}

	{
		"id" : "author2",
		"name" : "Mike Pages",
		"address" : {
			"street" : "#2 Hamlet Drive",
			"city" : "Boston",
			"country" : "USA"
		},

		books: [
			{
				"id" : "authored_book1",
				"book_id" : "book1",
				"writtenOn" : "02/26/97"
			}
		]
	}

	E-commerce

	user - orders - one to many relationship and we can have a one way embedding wherein, the multiple orders can be embedded in our user

	{

		"id" : "uniqueId1",
		"username" : "saleMonster",
		"password" : "iLikeSavingMoney",
		"orders" : [
			{
				"id" : "uniqueItem1",
				"product_id" : "uniqueProdId1",
				"quantity" : 15,
				"subtotal" : 1500
			},
			{
				"id" : "uniqueItem2",
				"product_id" : "uniqueProdId2",
				"quantity" : 2,
				"subtotal" : 300
			}
		],

		"total" : 1800,
		"orderedOn" : "11/11/22",
		"status" : "delivered"
	}

	product 
	{
		"id" : "uniqueProd1",
		"name" : "lipTint",
		"price" : 100,
		"orders" : [
			{
				"id" : "uniqueProductOrder1",
				"order_id" : "uniqueOrderId1",
				"quantity" : 15,
				"subtotal" : 1500,
				"orderedOn" : "11/11/22",
				"status" : "delivered"
			}
		]
	}
	{
		"id" : "uniqueProd2",
		"name" : "Doritos",
		"price" : 150,
		"orders" : [
			{
				"id" : "uniqueProductOrder1",
				"order_id" : "uniqueOrderId1",
				"quantity" : 2,
				"subtotal" : 600,
				"orderedOn" : "11/11/22",
				"status" : "delivered"
			}
		]
	}

	user

	{
		username,
		password,
		orders: [
			{
				items: [
					{
						productId,
						quantity,
						subtotal
					}
				],
				total,
				orderedOn,
				status
			}
		]
	}

	product
	{
		name,
		price,
		orders: [
			{
				order_id,
				quantity,
				subtotal,
				orderedOn,
				status
			}
		]
	}